
# coding: utf-8

# ### Importing libraries

# In[45]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')
get_ipython().magic('matplotlib inline')


# Importing the data

# In[46]:


# Import the data
data = pd.read_csv('DougScore.csv')


# In[47]:


#inspecting the data
data.head()


# In[48]:


data.info()


# In[55]:


#setting categorical data to the right datatype
data.YearBuilt = data.YearBuilt.astype('category')
data.Brand = data.Brand.astype('category')
data.Type = data.Type.astype('category')
data.Town = data.Town.astype('category')
data.State = data.State.astype('category')
data.Countrybuilt = data.Countrybuilt.astype('category')


# In[75]:


data.info


# In[81]:


# Define the style
sns.set(style="darkgrid", palette="muted", color_codes=True)
plt.figure(figsize=(50,10))

# Plot the boxsplots
ax = sns.violinplot(data=data, x='Countrybuilt', y='DougScore', orient='v', color='lightgray', showfliers=False)
plt.setp(ax.artists, alpha=0.5)

# Add in points to show each observation
#sns.stripplot(x='Brand', y='DougScore', data=mov3, jitter=True, size=6, linewidth=0, hue = 'Studio', alpha=0.7)

ax.axes.set_title('Brands vs Dougscore',fontsize=30)
ax.set_xlabel('Brand',fontsize=20)
ax.set_ylabel('Dougscore',fontsize=20)

# Define where to place the legend
ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)


# In[72]:


plot1 = sns.lmplot(data=data, x='Totalweekend',y='TotalDaily',                  fit_reg=False, hue='Countrybuilt',size=10,)


# In[70]:


plot1 = sns.lmplot(data=data, x='Value',y='CoolFactor',                  fit_reg=False, hue='YearBuilt',size=10,)


# In[64]:


plot2 = sns.distplot(data["DougScore"], bins=5)
plot3 = sns.distplot(data["Totalweekend"], bins=5)
plot4 = sns.distplot(data["TotalDaily"], bins=5)


# In[71]:


plot5 = sns.distplot(data["YearBuilt"], bins=5)


# In[74]:


#Creating filters


# In[77]:


filter1BRAND = data[(data.Brand == 'Mercedes') | (data.Brand == 'Ferrari') | (data.Brand == 'Porsche') ]


# In[79]:


filter2COUNTRY = data[(data.Countrybuilt == 'Germany') | (data.Countrybuilt == 'Italy') | (data.Countrybuilt == 'USA') ]


# In[85]:


# Define the style
sns.set(style="darkgrid", palette="muted", color_codes=True)
plt.figure(figsize=(50,10))

# Plot the boxsplots
ax = sns.boxplot(data=filter1BRAND, x='Brand', y='DougScore', orient='v', color='lightgray', showfliers=False)
plt.setp(ax.artists, alpha=0.5)

# Add in points to show each observation
sns.stripplot(x='Brand', y='DougScore', data=filter2COUNTRY, jitter=True, size=6, linewidth=0, hue = 'Countrybuilt', alpha=0.7)

ax.axes.set_title('Brands vs Dougscore',fontsize=30)
ax.set_xlabel('Brand',fontsize=20)
ax.set_ylabel('Dougscore',fontsize=20)

# Define where to place the legend
ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

